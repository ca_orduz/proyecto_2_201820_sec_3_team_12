package model.vo;

public class Contador {
	private int valor;
	public Contador() {
		valor = 0;
	}
	public void anadir() {
		valor++;
	}
	public void reducir() {
		valor--;
	}
	public int darValor() {
		return valor;
	}
}
