package model.vo;

public class Sector {
	private double minLa;
	private double minLo;
	private double maxLa;
	private double maxLo;
	private String id;
	
	public Sector(double pMinLa, double pMinLo, double pMaxLa, double pMaxLo) {
		minLa = pMinLa;
		minLo = pMinLo;
		maxLa = pMaxLa;
		maxLo = pMaxLo;
	}
	public double darMinLa() {
		return minLa;
	}
	public String darId() {
		return id;
	}
	public void asignarId(String pId) {
		id = pId;
	}
	public double darMinLo() {
		return minLo;
	}
	public double darMaxLa() {
		return maxLa;
	}
	public double darMaxLo() {
		return maxLo;
	}
	public boolean estaDentro(double pLat, double pLong) {
		boolean r = false;
		if(pLat<=maxLa) {
			if(pLat>=minLa) {
				if(pLong<=maxLo) {
					if(pLong>=minLo) {
						r = true;
					}
				}
			}
		}
		return r;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String ret = "("+minLa+","+minLo+") --> ("+maxLa+","+maxLo+")";
		return ret;
	}
}
