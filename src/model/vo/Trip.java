package model.vo;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Trip implements Comparable<Trip> {

    public final static String MALE = "Male";
    public final static String FEMALE = "Female";
    public final static String UNKNOWN = "unknown";
    public final static String INICIO = "inicio";
    public final static String TERMINACION = "terminacion";
    public final static String SUBSCRIPTOR = "sub";
    public final static String CUSTOMER = "cus";
    
    private int tripId;
    private LocalDate startTime;
    private LocalDate stopTime;
    private LocalDate tiempoC4;
    private String datoC4;
    
    private int bikeId;
    private int tripDuration;
    private int startStationId;
    private int endStationId;
    private String gender;
    private String startStationName;
    private String endStationName;
    private String esSub;
    public Trip(int tripId, LocalDate startTime, LocalDate stopTime, int bikeId, int tripDuration, int startStationId, int endStationId, String gender, String startName, String endName, String sub) {
        this.tripId = tripId;
        this.startTime = startTime;
        this.stopTime = stopTime;
        this.bikeId = bikeId;
        this.tripDuration = tripDuration;
        this.startStationId = startStationId;
        this.endStationId = endStationId;
        this.gender = gender;
        startStationName = startName;
        endStationName = endName;
        esSub = sub;
    }
    /**
     * Este constructor es para crear un viaje para el punto c4
     * @param pTripId el id del viaje
     * @param pBikeId el id de la bicicleta
     * @param tipoViaje si el viaje es de inicio o de terminacion 
     * @param laHora la hora a la que inicio/termino el viaje
     */
    public Trip(int pTripId, int pBikeId, String tipoViaje, LocalDate laHora) {
    	tripId = pTripId;
    	bikeId = pBikeId;
    	datoC4 = tipoViaje;
    	tiempoC4 = laHora;
    }
    public String toStringC4() {
    	return "Trip Id: "+tripId+", Bike Id: "+bikeId+", El viaje es de "+datoC4+". Hora "+datoC4+": "+tiempoC4+'\n'+"-----";
    }
    public String darStartName() {
    	return startStationName;
    }
    public String darEndName() {
    	return endStationName;
    }
    public String darDatoC4() {
    	return datoC4;
    }
    public LocalDate darTiempoC4() {
    	return tiempoC4;
    }
    public int compareTo2(Trip o) {
    	int r = 0;
    	if(tiempoC4.compareTo(o.darTiempoC4())>0) {
    		r = 1;
    	}
    	else if(tiempoC4.compareTo(o.darTiempoC4())<0) {
    		r = -1;
    	}
    	return r;
    }
    @Override
    public int compareTo(Trip o) {
    	// TODO completar
        if(startTime.isAfter(o.getStartTime())){
        	return 1;
        }else if(startTime.isBefore(o.getStartTime())){
        	return -1;
        }
        return 0;
    }
    public int compareTo3(Trip o) {
    	// TODO completar
        if(tripDuration>o.tripDuration){
        	return 1;
        }else if(tripDuration<o.tripDuration){
        	return -1;
        }
        return 0;
    }

    public int getTripId() {
        return tripId;
    }

    public LocalDate getStartTime() {
        return startTime;
    }

    public LocalDate getStopTime() {
        return stopTime;
    }

    public int getBikeId() {
        return bikeId;
    }

    public int getTripDuration() {
        return tripDuration;
    }

    public int getStartStationId() {
        return startStationId;
    }

    public int getEndStationId() {
        return endStationId;
    }

    public String getGender() {
        return gender;
    }
    public String darSub() {
    	return esSub;
    }
    @Override
    public String toString() {
    	// TODO Auto-generated method stub
    	return getTripId() + ", " + startStationName+", " + endStationName +", "+ getStartTime()+","+getStopTime();
    }
}
