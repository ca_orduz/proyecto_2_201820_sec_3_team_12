package model.data_structures;
/*
 * Nota: La implementacion de esta clase fue sacada del libro Algorithms de Robert SedgeWick y Kevin Wayne
 */
public class SeparateChainingHash <K extends Comparable<K>,V>{
	private static final int CAPACIDAD_INICIAL= 23;
	
	private int paresLlaveValor;
	
	private int tamanioTabla;
	
	private BusquedaSecuencial<K, V>[] arreglito;
	
	public SeparateChainingHash() {
		this(CAPACIDAD_INICIAL);
	}
	public SeparateChainingHash(int pTamanio) {
		tamanioTabla = pTamanio;
		arreglito = (BusquedaSecuencial<K,V>[]) new BusquedaSecuencial[pTamanio];
		for(int i=0;i<pTamanio;i++) {
			arreglito[i] = new BusquedaSecuencial<K,V>();
		}
	}
	public void resize(int cadenas) {
		SeparateChainingHash<K, V> temp = new SeparateChainingHash<K,V>();
		for(int i=0;i<tamanioTabla;i++) {
			for(K llavecita : arreglito[i].keys()) {
				temp.put(llavecita, arreglito[i].get(llavecita));
			}
		}
		tamanioTabla = temp.tamanioTabla;
		paresLlaveValor = temp.paresLlaveValor;
		arreglito = temp.arreglito;
	}
	public int hash(K llavecita) {
		return ((llavecita.hashCode() & 0x7fffffff)%tamanioTabla);
	}
	public int size() {
		return paresLlaveValor;
	}
	public boolean isEmpty() {
		return (size()==0);
	}
	public boolean contains(K llavecita)  {
		return (get(llavecita) != null);
	}
	public V get(K llavecita)  {
		
		int i = hash(llavecita);
		return arreglito[i].get(llavecita);
	}
	public void put(K llavecita,V valorcito) {
		if(valorcito == null) {
			delete(llavecita);
			return;
		}
		if(paresLlaveValor>=10*tamanioTabla)resize(2*tamanioTabla);
		
		int i = hash(llavecita);
		if(!arreglito[i].contains(llavecita)) paresLlaveValor++;
		arreglito[i].put(llavecita, valorcito);
	}
	public void delete(K llavecita) {
		int i = hash(llavecita);
		if(arreglito[i].contains(llavecita))paresLlaveValor--;
		arreglito[i].delete(llavecita);
		if(tamanioTabla > CAPACIDAD_INICIAL && paresLlaveValor <= 2*tamanioTabla) resize(tamanioTabla/2);
	}
	public Iterable<K> keys(){
		Queue<K> queue = new Queue<K>();
		for(int i = 0;i<tamanioTabla;i++) {
			for(K llavecita : arreglito[i].keys()) {
				queue.enqueue(llavecita);
			}
		}
		return queue;
	}
	
}
