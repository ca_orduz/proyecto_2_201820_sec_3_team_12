package model.logic;

import API.IManager;
import controller.Controller.ResultadoCampanna;
import model.data_structures.ArregloFlexible;
import model.data_structures.ICola;
import model.data_structures.ILista;
import model.data_structures.LinearProbingHash;
import model.data_structures.Node;
import model.data_structures.Queue;
import model.data_structures.RedBlackBST;
import model.data_structures.SeparateChainingHash;
import model.data_structures.BusquedaSecuencial;
import model.vo.Bike;
import model.vo.BikeRoute;

import model.vo.Sector;
import model.vo.Station;
import model.vo.Trip;

import java.io.FileReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.opencsv.CSVReader;
import com.sun.org.apache.bcel.internal.generic.NEW;

public class Manager implements IManager {
	public static final String RUTA_JSON = "data/CDOT_Bike_Routes_2014_1216.json";
	//Ruta del archivo de datos 2017-Q1
	// TODO Actualizar
	public static final String TRIPS_Q1 = "data/Divvy_Trips_2017_Q1.csv";

	//Ruta del archivo de trips 2017-Q2
	// TODO Actualizar	
	public static final String TRIPS_Q2 = "data/Divvy_Trips_2017_Q2.csv";

	//Ruta del archivo de trips 2017-Q3
	// TODO Actualizar	
	public static final String TRIPS_Q3 = "data/Divvy_Trips_2017_Q3.csv";

	//Ruta del archivo de trips 2017-Q4
	// TODO Actualizar	
	public static final String TRIPS_Q4 = "data/Divvy_Trips_2017_Q4.csv";

	//Ruta del archivo de trips 2017-Q1-Q4
	// TODO Actualizar	
	public static final String TRIPS_Q1_Q4 = "Ruta Trips 2017-Q1-Q4 en directorio data";

	//Ruta del archivo de stations 2017-Q1-Q2
	// TODO Actualizar	
	public static final String STATIONS_Q1_Q2 = "data/Divvy_Stations_2017_Q1Q2.csv";

	//Ruta del archivo de stations 2017-Q3-Q4
	// TODO Actualizar	
	public static final String STATIONS_Q3_Q4 = "data/Divvy_Stations_2017_Q3Q4.csv";
	//Formato de la fecha
	public static final String DATE_PATTERN = "MM/dd/yyyy HH:mm:ss";

	public static final int RADIO_TIERRA = 6371;

	private RedBlackBST<String,Bike> bicisPorTiempo;
	private LinearProbingHash<String, RedBlackBST<String, Trip>> paraElB2;
	private RedBlackBST arbol;
	private RedBlackBST arbolTrips=new RedBlackBST<>();
	private LinearProbingHash<Integer, Station> hashStats=new LinearProbingHash<>();
	private Queue tripQueue;
	private Queue statQueue;
	private ArregloFlexible<Trip> arregloTrips;
	private ArregloFlexible<Station> arregloStats;
	private ArregloFlexible<Station> arregloBikes;
	private ArrayList<BikeRoute> muestra=new ArrayList<BikeRoute>();
	private int index;
	private int tamStack;
	private ArrayList[] ciclorutas;
	private double minLat;
	private double maxLat;
	private double minLong;
	private double maxLong;
	private Sector[][] sectores;
	private ArrayList<String> idCiclovias;
	private ArrayList<String> coordenadas;
	private Sector buscado = null;

	public void cargarDatos(String rutaTrips, String rutaStations, String rutaJson) {
		cargarViajes(rutaTrips, rutaStations);
		cargarJson(rutaJson);
	}
	public void cargarJson(String rutaJson) {
		idCiclovias = new ArrayList<String>();
		coordenadas = new ArrayList<String>();
		maxLat = 0;
		maxLong= -100;
		minLat = 100;
		minLong = 0;
		Gson gson=new Gson();
		JsonReader reader;
		try {
			reader = new JsonReader(new FileReader(rutaJson));

			ciclorutas=gson.fromJson(reader, ArrayList[].class);
			System.out.println(ciclorutas.length);
			for(int i=0;i<ciclorutas.length;i++){
				muestra.add(new BikeRoute((String)ciclorutas[i].get(1), (String)ciclorutas[i].get(8),(String)ciclorutas[i].get(9),(String) ciclorutas[i].get(11),(String) ciclorutas[i].get(12), (String)ciclorutas[i].get(15)));
				String geom = muestra.get(i).darGeom();
				String infoo = geom.substring(12);
				String info = infoo.substring(0,infoo.length()-1);
				String[] pares = info.split(", ");
				idCiclovias.add(muestra.get(i).darId());
				idCiclovias.add(muestra.get(i).darId());
				String coordIniciales = pares[0];
				String coordFinales = pares[pares.length-1];
				coordenadas.add(coordIniciales);
				coordenadas.add(coordFinales);
				for(int j = 0;j<pares.length;j++) {
					String[] parAct = pares[j].split(" ");
					double longAct = Double.parseDouble(parAct[0]);
					double latAct = Double.parseDouble(parAct[1]);
					if(longAct<minLong) {
						minLong = longAct;
					}
					if(longAct>maxLong) {
						maxLong = longAct;
					}
					if(latAct<minLat) {
						minLat = latAct;
					}
					if(latAct>maxLat) {
						maxLat = latAct;
					}

				}

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void cargarViajes(String rutaTrips, String rutaStations) {
		try {


			String[] trip;
			String[] stat;


			arregloStats = new ArregloFlexible<Station>(600);
			arregloTrips = new ArregloFlexible<Trip>(1800000);
			tripQueue = new Queue();
			statQueue = new Queue();
			DateTimeFormatter formato = DateTimeFormatter.ofPattern(DATE_PATTERN);
			int cont = 0;
			for(int i=0;i<rutaStations.split(":").length;i++){
				CSVReader lectorStations = new CSVReader(new FileReader(rutaStations.split(":")[i]));
				stat = lectorStations.readNext();
				while((stat = lectorStations.readNext()) != null) {
					int id = Integer.parseInt(stat[0]);
					String statName = stat[1];
					int cp=Integer.parseInt(stat[5]);
					String[]fecha=stat[6].split("(?=\\s)");
					double lat = Double.parseDouble(stat[3]);
					double lon = Double.parseDouble(stat[4]);
					LocalDate startDate = convertirFecha_Hora_LDT(fecha[0], fecha[1]);
					Station estacion = new Station(id, statName, startDate, lat, lon,cp);
					String info = estacion.toString();
					Node nodo = new Node<>(estacion);

					//statQueue.enqueue(nodo);
					arregloStats.agregarElem(estacion);

					cont++;
				}
			}
			int cont2 = 0;
			for(int i=1;i<rutaTrips.split(":").length;i++){
				CSVReader lectorTrips = new CSVReader(new FileReader(rutaTrips.split(":")[i]));
				trip = lectorTrips.readNext();
				while((trip = lectorTrips.readNext()) != null) {
					int tripID = Integer.parseInt(trip[0]);
					String[] fecha=trip[1].split("(?=\\s)");
					String[] fecha1=trip[2].split("(?=\\s)");
					LocalDate startTime = convertirFecha_Hora_LDT(fecha[0], fecha[1]);
					LocalDate endTime = convertirFecha_Hora_LDT(fecha1[0], fecha1[1]);
					int bikeID = Integer.parseInt(trip[3]);
					int tripDuration = Integer.parseInt(trip[4]);
					int startStatID = Integer.parseInt(trip[5]);
					int endStatID = Integer.parseInt(trip[7]);
					String startName =trip[6];
					String endName=trip[8];
					String gender;
					if(trip[10].equals(Trip.MALE))
						gender = Trip.MALE;
					else if(trip[10].equals(Trip.FEMALE))
						gender = Trip.FEMALE;
					else
						gender = Trip.UNKNOWN;

					String sub = trip[9];
					if(sub.equals("Subscriber")) {
						sub = Trip.SUBSCRIPTOR;
					}
					else {
						sub = Trip.CUSTOMER;
					}
					Trip viaje = new Trip(tripID, startTime, endTime, bikeID, tripDuration, startStatID, endStatID, gender,startName, endName,sub);
					Node nodo = new Node<>(viaje);
					//tripQueue.enqueue(nodo);
					arregloTrips.agregarElem(viaje);

					cont2++;
				}
			}
			System.out.println("Total trips cargados en el sistema: " + cont2);
			System.out.println("Total estaciones cargadas en el sistema: "+ cont);
			darArbolBicicletas();
			for(int i=0;i<arregloTrips.darTamAct()-1;i++){
				arbolTrips.put(((Trip)(arregloTrips.darElementoI(i))).getStopTime(), ((Trip)arregloTrips.darElementoI(i)));


			}
			for(int i=0;i<arregloStats.darTamAct()-1;i++){
				hashStats.put(((Station)arregloStats.darElementoI(i)).getStationId(), ((Station)arregloStats.darElementoI(i)));
			}

			System.out.println("Numero de bicicletas en el arbol: "+arbol.size());
			System.out.println("Altura del arbol: " +arbol.height());
			cargarBicisEnArbolB1();
			cargarParaB2();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}


	}
	private static LocalDate convertirFecha_Hora_LDT(String fecha, String hora)
	{
		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[0]);
		int dia = Integer.parseInt(datosFecha[1]);
		int horas = Integer.parseInt(datosHora[0].trim());
		int minutos = Integer.parseInt(datosHora[1]);
		int segundos = 0;

		if(datosHora.length>2){
			segundos=Integer.parseInt(datosHora[2]);
		}

		return LocalDate.of(agno, mes, dia);
	}


	public double distanciaViaje(Trip trip){
		double longitudStart=0;
		double longitudEnd=0;
		double latitudStart=0;
		double latitudEnd=0;
		double distancia=0;
		for(int i=0;i<arregloStats.darTamAct();i++){
			if(((Station)arregloStats.darElementoI(i)).getStationId()==trip.getStartStationId()){
				latitudStart=((Station)arregloStats.darElementoI(i)).getLat();
				longitudStart=((Station)arregloStats.darElementoI(i)).getLong();
			}
			if(((Station)arregloStats.darElementoI(i)).getStationId()==trip.getEndStationId()){
				latitudEnd=((Station)arregloStats.darElementoI(i)).getLat();
				longitudEnd=((Station)arregloStats.darElementoI(i)).getLong();
			}
		}
		distancia=distancia(latitudStart, longitudStart, latitudEnd, longitudEnd);
		return distancia;
	}


	public double distancia(double lat1, double long1, double lat2, double long2) {
		double deltaLat = Math.toRadians((lat2-lat1));
		double deltaLong = Math.toRadians((long2-long1));

		lat1 = Math.toRadians(lat1);
		lat2 = Math.toRadians(lat2);
		double a = harvesin(deltaLat) + Math.cos(lat1)*Math.cos(lat2)*harvesin(deltaLong);
		double c = 2*Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		return 1000*RADIO_TIERRA*c;
	}
	public double harvesin(double valor) {
		return Math.pow(Math.sin(valor / 2), 2);
	}
	public void darArbolBicicletas(){
		arbol=new RedBlackBST();
		arregloBikes=new ArregloFlexible<>(1000000);
		for(int i =0;i<arregloTrips.darTamAct();i++){
			int duracion=0;
			int viajes=0;
			boolean existe=false;
			if(arregloBikes.darTamAct()!=0){
				for(int j=0;j<arregloBikes.darTamAct();j++){
					if(((Trip)arregloTrips.darElementoI(i)).getBikeId()==((Bike)arregloBikes.darElementoI(j)).getBikeId()){
						((Bike)arregloBikes.darElementoI(j)).agregarViaje();
						((Bike)arregloBikes.darElementoI(j)).aumentarDuracion(((Trip)arregloTrips.darElementoI(i)).getTripDuration());
						((Bike)arregloBikes.darElementoI(j)).aumentarDistancia(distanciaViaje(((Trip)arregloTrips.darElementoI(i))));
						existe=true;
					}
				}

			}
			if (existe==false){
				arregloBikes.agregarElem(new Bike(((Trip)arregloTrips.darElementoI(i)).getBikeId(), 1, distanciaViaje(((Trip)arregloTrips.darElementoI(i))), ((Trip)arregloTrips.darElementoI(i)).getTripDuration()));


			}
		}
		for(int c=0;c<arregloBikes.darTamAct();c++){
			arbol.put(((Bike)arregloBikes.darElementoI(c)).getBikeId(), "Viajes totales: "+ ((Bike)arregloBikes.darElementoI(c)).getTotalTrips()+", Distancia total recorrida: "+((Bike)arregloBikes.darElementoI(c)).getTotalDistance()+", Total de tiempo: " +((Bike)arregloBikes.darElementoI(c)).getTotalDuration());

		}
		int suma=0;
		for(int l=0;l<arregloBikes.darTamAct();l++){
			suma+=(arbol.getHeight(((Bike)arregloBikes.darElementoI(l)).getBikeId()));
		}
		System.out.println("Altura de busqueda promedio: "+suma/arregloBikes.darTamAct());
	}
	public void consultarBicicleta(int id){

		if(arbol.get(id)!=null){
			System.out.println(arbol.get(id).toString());
		}else{
			System.out.println("No existe ninguna bicicleta con el id espeficicado.");
		}
	}
	public void consultarBicicletasRango(int id1,int id2){
		if(arbol.keys(id1, id2)!=null){
			for(Object key:arbol.keys(id1, id2)){
				System.out.println(key.toString());
			}
		}else{
			System.out.println("No existe ninguna llave id en ese rango");
		}
	}

	//	public void mergeSort(ArregloFlexible<Node> aOrdenar, int tipoOrden, int tipoElem) {
	//		ArregloFlexible<Node> aux = new ArregloFlexible<Node>(aOrdenar.darTamAct());
	//		dividir(aOrdenar, aux, 0, aOrdenar.darTamAct()-1, tipoOrden, tipoElem);
	//	}
	//	public void dividir(ArregloFlexible<Node> aOrdenar, ArregloFlexible<Node> aux, int menor, int mayor, int tipoOrden, int tipoElem) {
	//		if(mayor<=menor) return;
	//		int medio = menor + (mayor-menor)/2;
	//		dividir(aOrdenar, aux, menor, medio, tipoOrden, tipoElem);
	//		dividir(aOrdenar, aux, medio+1, mayor, tipoOrden, tipoElem);
	//		juntar(aOrdenar, aux, menor, medio, mayor, tipoOrden, tipoElem);
	//	}
	//	public void juntar(ArregloFlexible<Node> aOrdenar, ArregloFlexible<Node> aux, int menor, int medio, int mayor, int tipoOrden, int tipoElem) {
	//		for(int i = menor;i<=mayor;i++) {
	//			aux.anadirElementoPos(i, aOrdenar.darElementoI(i));
	//		}
	//		int i = menor;
	//		int j = medio +1;
	//		for(int k = menor;k<= mayor;k++) {
	//			if(i>medio) {
	//				aOrdenar.anadirElementoPos(k, aux.darElementoI(j++));
	//			}
	//			else if(j>mayor) {
	//				aOrdenar.anadirElementoPos(k, aux.darElementoI(i++));
	//			}
	//			else if(aux.darElementoI(j).compareTo(aux.darElementoI(i).darElemento(), tipoElem, tipoOrden) <0) {
	//				aOrdenar.anadirElementoPos(k, aux.darElementoI(j++));
	//			}
	//			else {
	//				aOrdenar.anadirElementoPos(k, aux.darElementoI(i++));
	//			}
	//		}
	//	}


	@Override
	public Queue<Trip> A1(int n, LocalDate fechaTerminacion) {
		// TODO Auto-generated method stub
		Queue cola=new Queue();
		if(arbolTrips.get(fechaTerminacion)!=null){
			if(((Station)(hashStats.get(((Trip)arbolTrips.get(fechaTerminacion)).getEndStationId()))).getCp()>=n){

				cola.enqueue(((Trip)arbolTrips.get(fechaTerminacion)));
				System.out.println(((Trip)arbolTrips.get(fechaTerminacion)).toString());
			}
		}

		return cola;
	}

	@Override
	public ArregloFlexible<Trip> A2(int n) {
		// TODO Auto-generated method stub
		LinearProbingHash tabla=new LinearProbingHash<>();
		int duracion=0;
		ArregloFlexible arr=new ArregloFlexible<>(100);
		for(int i=0;i<arregloTrips.darTamAct();i++){

			duracion= ((Trip)arregloTrips.darElementoI(i)).getTripDuration()/60;

			if( duracion>=(n-1)*2&& duracion<=((n-1)*2)+1){
				arr.agregarElem(arregloTrips.darElementoI(i));
			}


			try{
				tabla.put(duracion, arr);
			}catch(Exception e){

			}

		}
		for(int i=0;i<arr.darTamAct();i++){
			System.out.println(((ArregloFlexible)tabla.get((n-1)*2)).darElementoI(i));
		}
		return (ArregloFlexible)tabla.get((n-1)*2);

	}

	@Override
	public ArregloFlexible<Trip> A3(int n, LocalDate fecha) {
		shellsort(arregloTrips);
		int contador=0;
		ArregloFlexible a=new ArregloFlexible(100);
		for(int i=arregloTrips.darTamAct();i>=0;i--){
			if(contador==n){
				break;
			}
			if(((Trip)arregloTrips.darElementoI(i)).getStartTime().equals(fecha)){
				a.agregarElem(((Trip)arregloTrips.darElementoI(i)));
			}
		}
		return a;
	}
	public void imprimirLatYLongMaxYMin() {
		System.out.println("minLat= " + minLat);
		System.out.println("maxLat= "+maxLat);
		System.out.println("minLong= "+minLong);
		System.out.println("maxLong= "+maxLong);
	}
	public void sectorizar(int la, int lo){
		sectores = new Sector[la][lo];
		double deltaLat = (maxLat-minLat)/la;
		double deltaLong = (maxLong-minLong)/lo;
		double latAct = minLat;
		double longAct = minLong;
		int c = 1;
		for(int i = 0;i<la;i++) {
			for(int j = 0;j<lo;j++) {
				sectores[i][j] = new Sector(latAct, longAct, latAct+deltaLat, longAct+deltaLong);
				latAct += deltaLat;
				System.out.println("Sector " + c + ": " +sectores[i][j].toString());
				sectores[i][j].asignarId(c+"");
				c++;
			}
			longAct += deltaLong;
		}
		imprimirLatYLongMaxYMin();
	}
	public <T> void shellsort(ArregloFlexible cola){

		int	n=cola.darTamano();


		long inicio=System.currentTimeMillis();

		for (int gap = n/2; gap > 0; gap /= 2)
		{

			for (int i = gap; i < n; i += 1)
			{

				Comparable temp = cola.elementos[i];

				// shift earlier gap-sorted elements up until the correct 
				// location for a[i] is found
				int j;            
				for (j = i; j >= gap && comparar(((Trip)cola.elementos[j-gap]),((Trip)temp))<0 ; j -= gap){

					cola.elementos[j]= cola.elementos[j-gap];

				}
				cola.elementos[j]=temp;
				//  put temp (the original a[i]) in its correct location

			}
		}

	}
	public int comparar(Trip t1, Trip t2){

		// TODO completar
		if(distanciaViaje(t1)>distanciaViaje(t2)){
			return 1;
		}else if(distanciaViaje(t1)<distanciaViaje(t2)){
			return -1;
		}
		return 0;

	}
	public void cargarBicisEnArbolB1() {
		bicisPorTiempo = new RedBlackBST<String,Bike>();
		ArregloFlexible<Bike> bicis = new ArregloFlexible<Bike>(804002);
		for(int i =0;i<arregloTrips.darTamAct();i++) {
			Trip viajeAct = (Trip) arregloTrips.darElementoI(i);
			int idAct = viajeAct.getBikeId();
			int dur = viajeAct.getTripDuration();
			boolean esta = false;
			for(int j=0;j<bicis.darTamAct()&&!esta;j++) {
				Bike bici = (Bike) bicis.darElementoI(j);
				if(bici.getBikeId()==idAct) {
					bici.agregarDuracion(dur);
					esta = true;
				}
			}
			if(!esta) {
				bicis.agregarElem(new Bike(idAct, 0, 0, dur));
			}
		}
		for(int i=0;i<bicis.darTamAct();i++) {
			Bike act = (Bike) bicis.darElementoI(i);
			int dur = act.getTotalDuration();
			bicisPorTiempo.put(dur+"", act);
		}

	}
	public void cargarParaB2() {
		paraElB2 =new LinearProbingHash<String,RedBlackBST<String, Trip>>();
		for(int i =0;i<arregloTrips.darTamAct();i++) {
			Trip tripAct = (Trip) arregloTrips.darElementoI(i);
			String startName = tripAct.darStartName();
			String endName = tripAct.darEndName();
			String dur = tripAct.getTripDuration()+"";
			String keyStats = startName+"-"+endName;
			//	System.out.println(keyStats);
			if(paraElB2.get(keyStats)==null) {
				RedBlackBST<String, Trip> temp = new RedBlackBST<String,Trip>();
				temp.put(dur, tripAct);
				try {
					paraElB2.put(keyStats, temp);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else {
				paraElB2.get(keyStats).put(dur, tripAct);
			}
		}
	}
	@Override
	public ILista<Bike> B1(int limiteInferior, int limiteSuperior) {

		// TODO Auto-generated method stub
		String tiempo1= limiteInferior+"";
		String tiempo2 = limiteSuperior+"";
		Queue<String> llaves=(Queue<String>) bicisPorTiempo.keys(tiempo1, tiempo2);
		if(bicisPorTiempo.keys(tiempo1, tiempo2)!=null){
			for(String a:llaves) {
				Bike bici= bicisPorTiempo.get(a);
				System.out.println(bici.toString());
			}
		}else{
			System.out.println("No existe ninguna llave id en ese rango");
		}

		return null;
	}


	@Override
	public ILista<Trip> B2(String estacionDeInicio, String estacionDeLlegada, int limiteInferiorTiempo, int limiteSuperiorTiempo) {
		// TODO Auto-generated method stub
		String key = estacionDeInicio+"-"+estacionDeLlegada;
		RedBlackBST<String, Trip> actual = paraElB2.get(key);
		if(actual != null) {
			if(actual.keys(limiteInferiorTiempo+"", limiteSuperiorTiempo+"")!=null) {
				Queue<String> llaves = (Queue<String>) actual.keys(limiteInferiorTiempo+"", limiteSuperiorTiempo+"");
				for(String a : llaves) {
					Trip viaje = actual.get(a);
					System.out.println(viaje.toString());
				}
			}
			else {
				System.out.println("Aiudaaaaa");
			}
		}
		else {
			System.out.println("No se encontraron viajes entre esas estaciones");
		}
		return null;
	}

	@Override
	public int[] B3(String estacionDeInicio, String estacionDeLlegada) {
		// TODO Auto-generated method stub
		int[] viajes = new int[24];
		for(int i=0;i<viajes.length;i++) {
			viajes[i]=0;
		}
		for(int i=0;i<arregloTrips.darTamAct();i++) {
			Trip actual = (Trip) arregloTrips.darElementoI(i);
			String ini = actual.darStartName();
			String end = actual.darEndName();
			if(ini.equals(estacionDeInicio)&&end.equals(estacionDeLlegada)) {
				String tiempo = actual.getStartTime().toString();
				//System.out.println(tiempo);
				String horaCompleta = tiempo.split("T")[1];
				//System.out.println(horaCompleta);
				String hora = horaCompleta.split(":")[0];
				int numHora = Integer.parseInt(hora);
				viajes[numHora]++;
			}
		}
		int hora =0;
		int highViajes=0;
		for(int i=0;i<viajes.length;i++) {
			if(viajes[i]>highViajes) {
				highViajes=viajes[i];
				hora = i;
			}
		}
		return new int[] {hora, highViajes};
	}
	public int darIndiceEstacionPorId(int id) {
		int indice = 0;
		boolean esta = false;
		for(int i=0;i<arregloStats.darTamAct()&&!esta;i++) {
			Station act = (Station) arregloStats.darElementoI(i);
			if(id == act.getStationId()) {
				indice=i;
				esta = true;
			}
		}
		return indice;
	}
	@Override
	public ResultadoCampanna C1(double valorPorPunto, int numEstacionesConPublicidad, int mesCampanna) {
		// TODO Auto-generated method stub
		double costo = 0;
		
		Contador[][] puntajes = new Contador[arregloStats.darTamAct()][12];
		Station[][] estaciones = new Station[puntajes.length][12];
		for(int i=0;i<puntajes.length;i++) {
			
			for(int j=0;j<12;j++) {
				estaciones[i][j] = (Station) arregloStats.darElementoI(i);
				puntajes[i][j] = new Contador();
			}
		}
		if(mesCampanna ==1||mesCampanna==12) {
			System.out.println("Por favor ingrese un mes de febrero a octubre (de 2 a 11)");
		}
		else
		{
			for(int i=0;i<arregloTrips.darTamAct();i++) {
				Trip actual = (Trip) arregloTrips.darElementoI(i);
				int idStart = actual.getStartStationId();
				int idEnd = actual.getEndStationId();
				String sub = actual.darSub();
				int indice1 = darIndiceEstacionPorId(idStart);
				int indice2 = darIndiceEstacionPorId(idEnd);
				String fecha1 = actual.getStartTime().toString();
				String fecha2 = actual.getStopTime().toString();
				int mes1 = Integer.parseInt(fecha1.split("/")[0]);
				int mes2 = Integer.parseInt(fecha2.split("/")[0]);
				puntajes[indice1][mes1].anadir();
				puntajes[indice2][mes2].anadir();
				if(sub.equals(Trip.SUBSCRIPTOR)) {
					puntajes[indice1][mes1].anadir();
					puntajes[indice2][mes2].anadir();
				}
			}
			//en este ciclo se ordenan las estaciones, solo en las filas de la matriz referentes a los tres meses
			for(int i=0;i<puntajes.length;i++) {
				for(int j=0;j<(puntajes.length-i-1);j++) {
					for(int k=-1;k<2;k++) {
						int pJ=puntajes[j][mesCampanna-k].darValor();
						int pJ1=puntajes[j+1][mesCampanna-k].darValor();
						if(pJ<pJ1) {
							Contador cTemp = puntajes[j][mesCampanna-k];
							puntajes[j][mesCampanna-k] = puntajes[j+1][mesCampanna-k];
							puntajes[j+1][mesCampanna-k]=cTemp;
							Station sTemp = estaciones[j][mesCampanna-k];
							estaciones[j][mesCampanna-k] = estaciones[j+1][mesCampanna-k];
							estaciones[j+1][mesCampanna-k] = sTemp;
						}
					}
				}
			}
			Station[][] estacionesFinales = new Station[numEstacionesConPublicidad][3];
			int puntosTotales= 0;
			for(int i=0;i<numEstacionesConPublicidad;i++) {
				for(int j=0;j<3;j++) {
					int k= j-1;
					estacionesFinales[i][j]=estaciones[i][mesCampanna-k];
					puntosTotales+=puntajes[i][mesCampanna-k].darValor();
				}
			}
			costo = valorPorPunto*puntosTotales/3;
			
			System.out.println("Total a pagar: " + costo);
			System.out.println("Estaciones");
			for(int i=0;i<numEstacionesConPublicidad;i++) {
				String aImprimir = estaciones[i][mesCampanna].getStationName();
				System.out.println(aImprimir);
			}
		}
		return null;
	}

	@Override
	public double[] C2(int LA, int LO) {
		sectorizar(LA, LO);
		return null;
	}

	@Override
	public int darSector(double latitud, double longitud) {
		
		int contador=0;
		for(int i = 0;i<sectores.length&& buscado != null;i++) {
			for(int j = 0;j<sectores[0].length && buscado != null;j++) {
				if(sectores[i][j].estaDentro(latitud, longitud)) {
					buscado = sectores[i][j];
				}
				contador++;
			}
		}
		System.out.println("Sector: "+contador);
		return contador;
	}

	@Override
	public ILista<Station> C3(double latitud, double longitud) {
		darSector(latitud,longitud);
		// TODO Auto-generated method stub
		
		for(int i=0;i<arregloStats.darTamAct();i++){
			if(((Station)arregloTrips.darElementoI(i)).getLat()>=buscado.darMinLa()&&((Station)arregloTrips.darElementoI(i)).getLat()<=buscado.darMaxLa()&&((Station)arregloTrips.darElementoI(i)).getLong()>=buscado.darMinLo()&&((Station)arregloTrips.darElementoI(i)).getLong()<=buscado.darMaxLo()){
				System.out.println(((Station)arregloTrips.darElementoI(i)).toString());
			}
		}
		return null;
	}

	@Override
	public ILista<BikeRoute> C4(double latitud, double longitud) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<BikeRoute> C5(double latitudI, double longitudI, double latitudF, double longitudF){
		// TODO Auto-generated method stub
		return null;
	}


}
